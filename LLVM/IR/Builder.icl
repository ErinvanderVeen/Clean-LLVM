implementation module LLVM.IR.Builder

import Control.Monad.State
import Data.Func
import Data.Functor.Identity
import LLVM.AST.Name
import StdInt
import StdOverloaded
import StdTuple

runBuilder :: !(IRBuilder a) -> a
runBuilder b = fst $ runState b defaultState

defaultState :: IRBuilderState
defaultState =
	{ IRBuilderState
	| nameCounter = 0
	}

freshName :: IRBuilder Name
freshName = state (\a=:{nameCounter} -> (IntName nameCounter, {a & nameCounter = nameCounter + 1}))
