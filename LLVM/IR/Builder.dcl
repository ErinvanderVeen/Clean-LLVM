definition module LLVM.IR.Builder

import Control.Monad.State
import LLVM.AST.Name

:: IRBuilder a :== State IRBuilderState a

:: IRBuilderState =
	{ nameCounter :: Int
	}

runBuilder :: !(IRBuilder a) -> a

freshName :: IRBuilder Name
