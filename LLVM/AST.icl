implementation module LLVM.AST

import LLVM.AST.Instruction
import LLVM.AST.Global
import LLVM.AST.Name
import LLVM.AST.Type
import StdFunctions
import StdMisc
import Text.Show

instance Show Definition
where
	showsPrec _ (GlobalDefinition g) = shows g
	show x = shows x ""
	showList [] = showString ""
	showList [a:as] = shows a o showChar '\n' o showList as

instance Show Module
where
	showsPrec _ m = shows m.moduleDefinitions
	show x = shows x ""
	showList _ = abort "LLVM.AST: Showing a list of Modules is unsupported"

defaultModule :: Module
defaultModule =
	{ Module
	| name = ""
	, sourceFileName = ""
	, moduleDefinitions = []
	}
