definition module LLVM.AST

import LLVM.AST.Instruction
import LLVM.AST.Global
import LLVM.AST.Name
import LLVM.AST.Type
import Text.Show

:: Definition
	= GlobalDefinition Global

instance Show Definition

:: Module =
	{ name :: String
	, sourceFileName :: String
	, moduleDefinitions :: [Definition]
	}

instance Show Module

defaultModule :: Module
