definition module LLVM.AST.Constant

from Text.Show import class Show

:: Constant
	// Integer of Size with Value
	= Int Int Int

instance Show Constant
