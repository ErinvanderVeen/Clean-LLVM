implementation module LLVM.AST.Constant

import StdFunctions
import StdMisc
import Text.Show

instance Show Constant
where
	showsPrec _ (Int s v) = showChar 'i' o shows s o showChar ' ' o shows v
	show x = shows x ""
	showList _ = abort "LLVM.AST.Constant: Showing a list of Constants is unsupported"
