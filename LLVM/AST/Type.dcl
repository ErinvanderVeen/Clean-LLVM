definition module LLVM.AST.Type

import Text.Show

:: Type
	= VoidType
	// The number of bits, i1, i8, i16 etc.
	| IntType Int
	| LabelType

instance Show Type

// Aliases for frequently used types
i1 :: Type
i8 :: Type
char :: Type
i64 :: Type
