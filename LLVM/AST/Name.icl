implementation module LLVM.AST.Name

import StdFunctions
import StdMisc
import Text.Show

instance Show Name
where
	showsPrec _ (Name s) = showString s
	showsPrec _ (IntName i) = showChar 'l' o shows i
	showsPrec _ UnNamed = showString ""
	show x = shows x ""
	showList _ = abort "LLVM.AST.Name: Showing a list of Names is unsupported"
