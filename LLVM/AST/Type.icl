implementation module LLVM.AST.Type

import StdFunctions
import Text.Show

instance Show Type
where
	showsPrec _ VoidType = showString "void"
	showsPrec _ (IntType i) = showChar 'i' o shows i
	showsPrec _ LabelType = showString "label"
	show x = shows x ""
	showList [] = showString ""
	showList [t:ts] = shows t o showString ", " o showList ts

i1 :: Type
i1 = IntType 1

i8 :: Type
i8 = IntType 8

char :: Type
char = i8

i64 :: Type
i64 = IntType 64
