implementation module LLVM.AST.Instruction

import LLVM.AST.Name
import LLVM.AST.Operand
import LLVM.AST.Type
import StdFunctions
import StdMisc
import Text.Show

instance Show Instruction
where
	showsPrec _ (Call t n ops) = showString "call " o shows t o showString " @" o shows n o
		showChar '(' o showList ops o showChar ')'
	show x = shows x ""
	showList _ = abort "LLVM.AST.Instruction: Showing a list of Instructions is unsupported"

instance Show Terminator
where
	showsPrec _ (Br n) = showString "\tbr label %" o shows n
	showsPrec _ (CondBr op t f) = showString "\tbr " o shows op o
		showString ", label %" o shows t o showString ", label %" o shows f
	showsPrec _ (Ret ?None) = showString "\tret void"
	showsPrec _ (Ret (?Just v)) = showString "\tret " o shows v
	show x = shows x ""
	showList _ = abort "LLVM.AST.Instruction: Showing a list of Terminators is unsupported"

instance Show (Assigned a) | Show a
where
	showsPrec _ (Assigned n x) = showString "\t%" o shows n o showString " = " o shows x o showChar '\n'
	showsPrec _ (Unassigned x) = showChar '\t' o shows x o showChar '\n'
	show x = shows x ""
	showList [] = showString ""
	showList [a:as] = shows a o showList as
