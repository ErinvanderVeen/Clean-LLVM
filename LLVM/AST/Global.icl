implementation module LLVM.AST.Global

import LLVM.AST.Instruction
import LLVM.AST.Linkage
import LLVM.AST.Name
import LLVM.AST.Type
import StdFunctions
import StdMisc
import Text.Show

instance Show Global
where
	showsPrec _ (Function fd) = shows fd
	show x = shows x ""
	showList [] = showString ""
	showList [x:xs] = shows x o showChar '\n' o showList xs

instance Show GlobalVariableData
where
	showsPrec _ {name, linkage, isConstant, type} =
		showString "@" o shows name  o showString " = " o shows linkage o
			(if isConstant (showString "constant") (showString "global")) o shows type o
			showString"\n"
	show x = shows x ""
	showList _ = abort "LLVM.AST.Global: Showing a list of Global Variables is unsupported"

instance Show FunctionData
where
	showsPrec _ {linkage, returnType, name, parameters, basicBlocks=[]} =
		showString "declare " o shows linkage o shows returnType o showString " @" o shows name o
			showChar '(' o showList parameters o showString ")\n"
	showsPrec _ {linkage, returnType, name, parameters, basicBlocks} =
		showString "define " o shows linkage o shows returnType o showString " @" o shows name o
			showChar '(' o showList parameters o showString ") {\n" o
		showList basicBlocks o
		showString "}\n"
	show x = shows x ""
	showList _ = abort "LLVM.AST.Global: Showing a list of Functions is unsupported"

instance Show Parameter
where
	showsPrec _ (Parameter t n) = shows t o showChar ' ' o shows n
	show x = shows x ""
	showList [] = showString ""
	showList [p] = shows p
	showList [p:ps] = shows p o showString ", " o showList ps

instance Show BasicBlock
where
	showsPrec _ (BasicBlock n is t) = shows n o showString ":\n" o showList is o shows t o showChar '\n'
	show x = shows x ""
	showList [] = showString ""
	showList [b:bs] = shows b o showChar '\n' o showList bs

defaultFunction :: FunctionData
defaultFunction =
	{ FunctionData
	| linkage = External
	, returnType = abort "LLVM.AST.Global: Function returntype undefined"
	, name = abort "LLVM.AST.Global: Function name undefined"
	, parameters = []
	, basicBlocks = []
	}
