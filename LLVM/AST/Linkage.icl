implementation module LLVM.AST.Linkage

import StdMisc
import Text.Show

instance Show Linkage
where
	showsPrec _ External = showString "external"
	show x = shows x ""
	showList _ = abort "LLVM.AST.Linkage: Showing a list of Linkage is unsupported"
