definition module LLVM.AST.Operand

import LLVM.AST.Constant
import LLVM.AST.Name
import LLVM.AST.Type
import Text.Show

:: Operand
	= Local Type Name
	| Constant Constant

instance Show Operand
