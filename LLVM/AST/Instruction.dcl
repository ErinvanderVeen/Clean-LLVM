definition module LLVM.AST.Instruction

import LLVM.AST.Name
import LLVM.AST.Operand
import LLVM.AST.Type
import Text.Show

:: Instruction
	= Call Type Name [Operand]

instance Show Instruction

:: Terminator
	= Br Name
	| CondBr Operand Name Name
	| Ret (?Operand)

instance Show Terminator

/**
 * The result of an instruction can be assigned to a local variable
 */
:: Assigned a = Assigned Name a | Unassigned a

instance Show (Assigned a) | Show a
