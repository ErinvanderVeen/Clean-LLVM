definition module LLVM.AST.Global

import LLVM.AST.Instruction
import LLVM.AST.Linkage
import LLVM.AST.Name
import LLVM.AST.Type
import Text.Show

:: Global
	= GlobalVariable GlobalVariableData
	| Function FunctionData

instance Show Global

:: GlobalVariableData =
	{ name :: Name
	, linkage :: Linkage
	, isConstant :: Bool
	, type :: Type
	}

instance Show GlobalVariableData

:: FunctionData =
	{ linkage :: Linkage
	, returnType :: Type
	, name :: Name
	, parameters :: [Parameter]
	, basicBlocks :: [BasicBlock]
	}

instance Show FunctionData

:: Parameter = Parameter Type Name

instance Show Parameter

:: BasicBlock = BasicBlock Name [Assigned Instruction] Terminator

instance Show BasicBlock

defaultFunction :: FunctionData
