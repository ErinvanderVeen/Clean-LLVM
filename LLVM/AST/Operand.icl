implementation module LLVM.AST.Operand

import LLVM.AST.Constant
import LLVM.AST.Name
import LLVM.AST.Type
import StdFunctions
import Text.Show

instance Show Operand
where
	showsPrec _ (Local t n) = shows t o showString " %" o shows n
	showsPrec _ (Constant c) = shows c
	show x = shows x ""
	showList [] = showString ""
	showList [op] = shows op
	showList [op:ops] = shows op o showString ", " o showList ops
