definition module LLVM.AST.Name

import Text.Show

:: Name
	= Name String
	| IntName Int
	| UnNamed

instance Show Name
